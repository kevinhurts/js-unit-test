"use strict";

var gulp = require('gulp');

var karma = require('karma').server;

var safari = require('karma-safari-launcher');
var ie = require('karma-ie-launcher');
var firefox = require('karma-firefox-launcher');
var opera = require('karma-opera-launcher');

/*
** JS unit test files
    one could also externalize common config into a separate file,
    ex.: var karmaCommonConf = require('./karma-common-conf.js');
*/
var karmaCommonConf = {
  // base path that will be used to resolve all patterns (eg. files, exclude)
  basePath: '',
  //browsers: ['Chrome', 'Safari', 'Firefox', 'PhantomJS', 'IE'],
  browsers: ['PhantomJS'],
  frameworks: ['mocha', 'requirejs', 'chai'],
  files: [
        {pattern: 'lib/**/*.js', included: false},
        {pattern: 'js/**/*.js', included: false},
        {pattern: 'tests/**/*.spec.js', included: false},

        'karma-require.js',
  ],
  reporters: ['progress']
};

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
  karma.start(_.assign({}, karmaCommonConf, {singleRun: true}), done);
});

/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('tdd', function (done) {
  karma.start(karmaCommonConf, done);
});